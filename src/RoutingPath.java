import java.io.Serializable;
/**
 * Routing Path class:
 * 		- via is first-hop router
 * 		- contains cost to a certain destination 	
 * 		- used by printRoutingInfo() to print out lowest cost 
 * @author Annie Sun (z5075255)
 */
public class RoutingPath implements Serializable {
	public RoutingPath(char to, char via, float cost) {
		this.to = to;
		this.via = via;
		this.cost = cost;
	}
	
	public char getTo() {
		return this.to;
	}
	
	public char getVia() {
		return this.via;
	}

	public float getCost() {
		return this.cost;
	}

	private char to;
	private char via;
	private float cost;
}
