import java.io.Serializable;
import java.util.*;
/**
 * DV class:
 * 		- used by Router to calculate minimal cost to destination
 * 		- contains a list of costs to a certain destination from id 
 * @author Annie Sun (z5075255)
 *
 */

public class DV implements Serializable {
	public DV (char id, HashMap<Character, Float> costs) {
		this.id = id;
		this.costs = costs;		
	}
	
	public char getID() {
		return this.id;
	}
	
	public HashMap<Character, Float> getCosts () {
		return this.costs;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (this.getClass() != o.getClass()) {
			return false;
		}
		
		DV obj = (DV) o;		
		if (id != obj.id) {
			return false;
		} 
		
		for (Character c: obj.costs.keySet()) {
			if (!this.costs.containsKey(c) || !obj.costs.get(c).equals(this.costs.get(c))) {
				return false;
			}
		}
		
		for (Character c: obj.costs.keySet()) {
			if (!obj.costs.containsKey(c) || !this.costs.get(c).equals(obj.costs.get(c))) {
				return false;
			}
		}	
		return true;
	}
	
	private char id;
	private HashMap<Character, Float> costs;
}
