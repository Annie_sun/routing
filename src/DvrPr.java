import java.io.*;
import java.util.*;

/**
 * DvrPr class:
 * 	- contains main
 * 	- responsible for parsing in input 
 * 	- determines whether it runs in basic or poisoned reverse mode
 * 	- creates neighbours (id, port, cost, convergedCost)  
 * 			- converged cost is specific for poisoned reverse where link costs changes
 * 			- otherwise converged cost is identical to cost for basic
 * 	- creates router (id, port, number of neighbours, list of neighbours and mode)
 * @author Annie Sun (z5075255)
 *
 */

public class DvrPr {
	
	public static void main(String[] args) throws Exception {

		char nodeID;
		int nodePort;
		int numNeighbours;
		boolean poisonedFlag;

		if (!(args.length == 3 || args.length == 4)) {
			System.err.println("Usage: java DvrPr Node_ID NODE_PORT CONFIG_TXT [-p] ");
			System.exit(1);
		}

		nodeID = args[ID].charAt(ID);		
		if (nodeID < 'A' || nodeID > 'Z' ) {
			System.err.println("NODE_ID must be a single uppercase.");
			System.exit(1);
		}

		nodePort = Integer.parseInt(args[PORT]);

		if (args.length == 4) {
			poisonedFlag = true;
		} else {
			poisonedFlag = false;
		}

		Scanner neighbourInfo = new Scanner(new FileReader(args[CONFIG_TXT]));	

		numNeighbours = neighbourInfo.nextInt();
		HashMap<Character, Neighbour> neighbours = new HashMap<Character, Neighbour>();

		char neighbourID; 
		int neighbourPort;
		float cost, convergedCost;
		Neighbour n;
		
		try {
			while (neighbourInfo.hasNext())  {
				if (!poisonedFlag) {
					// basic format: NODE_ID COST PORT 
					neighbourID = neighbourInfo.next().charAt(ID);
					cost = neighbourInfo.nextInt();
					neighbourPort = neighbourInfo.nextInt();
					n = new Neighbour(neighbourID, neighbourPort, cost, cost);
					neighbours.put(neighbourID, n);
					
				} else {
					// poisoned reverse format: NODE_ID COST CONVERGED_COST PORT
					neighbourID = neighbourInfo.next().charAt(ID);
					cost = neighbourInfo.nextInt();
					convergedCost = neighbourInfo.nextInt();
					neighbourPort = neighbourInfo.nextInt();
					n = new Neighbour(neighbourID, neighbourPort, cost, convergedCost);
					neighbours.put(neighbourID, n);
				}	
			}
			
			new Router(nodeID, nodePort, numNeighbours, neighbours, poisonedFlag);	
				
		} catch (Exception e) {	
			e.printStackTrace();
		}
	}
	private static final int ID = 0;
	private static final int PORT = 1;
	private static final int CONFIG_TXT = 2;
}
