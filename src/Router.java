import java.util.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.io.*;

/**
 * Router class:
 * 		- creates a router with specified id, port , neighbours and mode
 * 		- creates a routing table 
 * 		- determines minimal cost to a destination (either basic or poisoned depending on flag)
 * 		- creates a thread listener to listen to incoming packets 
 * 		- prints routing information once it is stable
 * 		- creates a hashmap for stability to determine whether it is stable or not
 * 		- creates a hashmap for the time that last message was received; used to check whether 
 * 		  neighbour is still alive
 * @author Annie Sun (z5075255)
 *
 */
public class Router {

	public Router(char id, int port, int numNeighbours,
			HashMap<Character, Neighbour> neighbours, boolean flag) throws SocketException  {
		
		this.id = id;
		this.port = port;
		this.neighbours = neighbours;
		this.flag = flag;

		for (Neighbour n: neighbours.values()) {
			HashMap<Character, Float> costs = new HashMap<Character, Float>();
			routingTable.put(n.getID() , new DV(id, costs));
			lastInfo.put(n.getID(), System.currentTimeMillis());
			stability.put(n.getID(), 0);		
		}	
		routerSocket = new DatagramSocket(port);		
		new Thread(new Updates(this)).start();
		
		if (!flag) {
			basicRouting();
		} else {		
			poisonedReverseRouting();
		}
		heartbeatAlive();		
	}
	
	private void basicRouting() {
		updateTimer = new Timer();

		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					
					DV d = null;
					DatagramSocket socket = null;
					DatagramPacket  packet = null;
					InetAddress ipAddress = InetAddress.getLocalHost();

					ByteArrayOutputStream data = new ByteArrayOutputStream();
					ObjectOutputStream output = new ObjectOutputStream(data);

					synchronized (routingTable) {

						HashMap<Character, Float> costs = new HashMap<Character, Float>();

						for (RoutingPath rp: dvCalculation()) {
							costs.put(rp.getTo(), rp.getCost());
						}
						d = new DV(id, costs);

						output.writeObject(d);
					}

					packet = new DatagramPacket(data.toByteArray(), data.size());

					for (Neighbour n: neighbours.values()) {
						socket = new DatagramSocket();
						packet.setAddress(ipAddress);
						packet.setPort(n.getPort());
						socket.send(packet);
					}

				} catch (Exception e) {
					 
				}
			}
		}, 0, UPDATE_INTERVAL);  
	}

	private void poisonedReverseRouting() {
		updateTimer = new Timer();
		
		updateTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				for (Neighbour n: neighbours.values()) {
					try {
						
						ByteArrayOutputStream data = new ByteArrayOutputStream();
						ObjectOutputStream output = new ObjectOutputStream(data);
						DV d = null;
						DatagramPacket packet = null;
						DatagramSocket socket = null;
						InetAddress ipAddress = InetAddress.getLocalHost();
						
						synchronized (routingTable) {
							HashMap<Character, Float> costs = new HashMap<Character, Float>();

							for (RoutingPath rp: dvCalculation()){
								if (rp.getVia() == n.getID()) {
									costs.put(rp.getTo(), Float.MAX_VALUE);
								} else {
									costs.put(rp.getTo(), rp.getCost());
								}
							}
							d = new DV(id, costs);
							output.writeObject(d);
						}
						
						packet = new DatagramPacket(data.toByteArray(),data.size());
						socket = new DatagramSocket();
						packet.setAddress(ipAddress);
						packet.setPort(n.getPort());
						socket.send(packet);
						// socket.close();
					} catch (Exception e) {

					}
				}
			}
		}, 0, UPDATE_INTERVAL); 
	}


	private ArrayList<RoutingPath> dvCalculation() {

		HashSet<Character> routerSet = new HashSet<Character>();

		routerSet.addAll(neighbours.keySet());
		for (DV d: routingTable.values()) {
			routerSet.addAll(d.getCosts().keySet());
		}
		//  invalid entries: router to this.router or this.router to this.router
		routerSet.remove(this.id);	
		routerSet.removeAll(killedRouters);
		
		ArrayList<Character> routers = new ArrayList<Character>(routerSet);
		ArrayList<RoutingPath> result = new ArrayList<RoutingPath>();
		// sort the routers so it is easier to determine costs when printed
		Collections.sort(routers);
		
		for (Character r: routers) {

			char via = '?';
			float cost = Float.POSITIVE_INFINITY;

			if (neighbours.containsKey(r)) {
				via = r;
				cost = neighbourCost(r);
			}

			for (Character n: routingTable.keySet()) {
				boolean viaFound = routingTable.get(n).getCosts().containsKey(r);

				if (viaFound) {
					float newCost = (routingTable.get(n).getCosts().get(r) + neighbourCost(n));
					if (newCost < cost) {
						via = n;
						cost = newCost;
					}
				}
			}
			RoutingPath newRoute = new RoutingPath (r, via, cost);
			result.add(newRoute);
		}
		return result;	
	}
	
	private void heartbeatAlive() {
		heartBeat = new Timer();
		
		heartBeat.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				ArrayList<Neighbour> neighbourCopy = new ArrayList<Neighbour>(neighbours.values());
				
				for (Neighbour n: neighbourCopy) {
					long currTime = System.currentTimeMillis();
					long lastInfoTime = lastInfo.get(n.getID());
					if ((currTime - lastInfoTime) > DEAD_INTERVAL) {
					
						routingTable.remove(n.getID());
						neighbours.remove(n.getID());
						killedRouters.add(n.getID());
					}
				}
			}
		} , 0, HEARTBEAT_INTERVAL);			
	}

	public void isStabilised() {

		if (!stable) {

			printRoutingInfo();
		}
		isStable = true;
		stable = true;
	}

	private void printRoutingInfo() {

		ArrayList<RoutingPath> shortestPathInfo = new ArrayList <RoutingPath>();
		shortestPathInfo = dvCalculation();
		
		for (RoutingPath rp: shortestPathInfo) {
			System.out.println("shortest path to node " + rp.getTo() + " : the next hop is " + 
					rp.getVia() +  " and the cost is " + rp.getCost() );
		}
	}

	private float neighbourCost(Character router) {

		float cost = Float.POSITIVE_INFINITY;

		if (isStable) {
			cost = neighbours.get(router).getConvergedCost();
		} else {
			cost = neighbours.get(router).getCost();
		}
		return cost;
	}

	public Character getID() {
		return this.id;
	}

	public int getPort() {
		return this.port;
	}

	public HashMap<Character, Neighbour> getNeighbours () {
		return this.neighbours;
	}

	public DatagramSocket getRouterSocket() {
		return this.routerSocket;
	}

	public void setUnstable() {
		stable = false;
	}

	public boolean getIsStable() {
		return this.isStable;
	}

	public HashMap<Character, DV> getRoutingTable () {
		return this.routingTable;
	}

	public HashMap<Character, Integer> getStability () {
		return this.stability;
	}

	public HashMap<Character, Long> getLastInfo() {
		return this.lastInfo;
	}

	public List<Character> getKilledRouters () {
		return this.killedRouters;
	}

	@Override
	public String toString() {
		return "Router{flag" + flag + ", neighbours=" + neighbours + ", port=" + port + ", id=" + id + '}'; 
	}

	private static final int UPDATE_INTERVAL = 5000;
	private static final int HEARTBEAT_INTERVAL = 1000;
	private static final int DEAD_INTERVAL = 16000;

	private char id;
	private int port;
	private HashMap<Character, Neighbour> neighbours;
	private boolean flag;
	private final HashMap<Character, DV> routingTable = new HashMap<Character, DV>();
	private HashMap<Character, Integer> stability = new HashMap<Character, Integer>();
	private final HashMap<Character, Long> lastInfo = new HashMap<Character, Long>();
	private List<Character> killedRouters = new ArrayList<Character>();

	private DatagramSocket routerSocket;
	private boolean stable = false;
	private boolean isStable = false;
	private Timer updateTimer;
	private Timer heartBeat;
}
