import java.net.*;
import java.util.*;
import java.io.*;

/**
 * Updates class:
 * 		- listens to incoming packets
 * 		- determines whether a network is stable or not
 * 		- notes the time last packet was received
 * @author Annie Sun (z5075255)
 *
 */
public class Updates implements Runnable {
	public Updates(Router router) {
		this.router = router;
	}

	byte[] data = new byte[PACKET_SIZE];
	DatagramPacket packet = new DatagramPacket(data, data.length);

	@Override
	public void run() {
		while (true) {
			try {	
				router.getRouterSocket().receive(packet);
				ByteArrayInputStream bytes = new ByteArrayInputStream(data);
				DV d = (DV) new ObjectInputStream(bytes).readObject();

				HashMap<Character, DV> routingTable = router.getRoutingTable();
				HashMap<Character, Integer> stability = router.getStability();

				synchronized (routingTable) {
					
					// no change in DV 
					if (!routingTable.get(d.getID()).equals(d)) {
						routingTable.put( d.getID(), d);
						stability.put((char) d.getID(), 1);		
						router.setUnstable();
					} else {
						Integer num = stability.get(d.getID()) + 1;	
						stability.put(d.getID(), num);
				
					}
				}
				// check for network stability
				boolean stable = true;
				for (Integer  i: stability.values()) {			
					if (i < 3) {		
						stable = false;
					}
				}				
				if (stable) {
					router.isStabilised();
				}
				// message time received
				router.getLastInfo().put((char) d.getID(), System.currentTimeMillis());

			} catch (Exception e) {
			}
		}
	}

	private Router router;
	private static final int PACKET_SIZE = 2048;
}
