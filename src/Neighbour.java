/**
 * Neighbour class:
 * 		- Contains id, port, cost to neighbour, converged cost to neighbour
 * @author Annie Sun (z5075255)
 *
 */
public class Neighbour {
	
	public Neighbour (char id, int port, float cost, float convergedCost) {
		this.id = id;
		this.port = port;
		this.cost = cost;
		this.convergedCost = convergedCost;
	}
	
	public char getID() {
		return this.id;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public float getCost() {
		return this.cost;
	}
	
	public float getConvergedCost() {
		return this.convergedCost;
	}
	
	private char id;
	private int port;
	private float cost; 
	private float convergedCost;
}
